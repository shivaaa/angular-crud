import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private tempArray:  Array<any> = [];
  forArray: Array<any> = [];
  showTable: boolean = false;
  options: number[];
  count:number=0;
// public totalArray=[];
// public transaction={
//   "id":"",
//   "value":"",
//   "transactionType":"",
// }
private fieldArray: Array<any> = [];
private againstArray: Array<any> = [];

private cancelArray: Array<any> = [];

private newAttribute: any = {};
  constructor() { }

  ngOnInit() {
    this.options = [1, 2, 3];
  }
  
  optionSelected: any;
  
  onOptionSelected(event){
   console.log(event); //option value will be sent as event
  }
  // newTransaction(){
  //   this.totalArray.push(this.transaction);
  //   console.log(this.totalArray);
  // }
  addFieldValue() {
    this.fieldArray.push(this.newAttribute)
    this.newAttribute = {};
}

deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
}
submitForm(){
this.cancelArray=[];
  console.log(this.fieldArray);
  // this.tempArray=this.fieldArray;
  for(let i=0;i<this.fieldArray.length;i++){
    this.tempArray[i]=this.fieldArray[i];
  }
  for(let i=0;i<this.fieldArray.length-1;i++){
    for(let j=i+1;j<this.fieldArray.length;j++){
      if(this.fieldArray[i].value==this.fieldArray[j].value){
      
      this.cancelArray.push(this.fieldArray[i]);
      this.removeArray(i,j-1,this.count);
      // this.tempArray.splice(i, 1);
      // this.tempArray.splice(j-1, 1);
      this.count++;
      }
    }
      }
      console.log(this.tempArray);
      console.log(this.fieldArray);
  for(let i=0;i<this.tempArray.length;i++){
    
    if(this.tempArray[i].tType!=null && this.tempArray[i].tType=='for' && this.forArray.indexOf(this.tempArray[i])==-1)
    this.forArray.push(this.tempArray[i]);
   else if(this.tempArray[i].tType!=null && this.tempArray[i].tType=='against'  && this.againstArray.indexOf(this.tempArray[i])==-1)
    this.againstArray.push(this.tempArray[i]);
  //  else if(this.fieldArray[i].tType!=null && this.fieldArray[i].tType=='cancel'  && this.cancelArray.indexOf(this.fieldArray[i])==-1)
  //   this.cancelArray.push(this.fieldArray[i]);
  }
 
  console.log(this.fieldArray);
  this.showTable=!this.showTable;
}
removeArray(i,j,count){
  this.tempArray.splice(i-count*2, 1);
  this.tempArray.splice(j-count*2, 1);
}
resetForm(){
  this.showTable=!this.showTable;
}

}
